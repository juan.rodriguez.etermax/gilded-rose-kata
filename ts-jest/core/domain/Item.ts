export default class Item {
    name: any;
    sellIn: any;
    quality: any;

    constructor(name, sellIn, quality){
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
    }
}
