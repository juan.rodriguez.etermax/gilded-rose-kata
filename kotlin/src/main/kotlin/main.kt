import gildedrose.itemRepository
import gildedrose.GildedRose

fun main(args: Array<String>) {
    println("OMGHAI!")

    val items = itemRepository()

    val app = GildedRose(items)

    var days = 2
    if (args.size > 0) {
        days = Integer.parseInt(args[0]) + 1
    }

    for (i in 0..days - 1) {
        println("-------- day $i --------")
        println("name, sellIn, quality")
        for (item in items) {
            println(item)
        }
        println()
        app.updateQuality()
    }

}