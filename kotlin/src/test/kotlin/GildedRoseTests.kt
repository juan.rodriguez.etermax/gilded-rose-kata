import gildedrose.Item
import gildedrose.GildedRose
import io.kotest.matchers.shouldBe
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import kotlin.test.Ignore

@Ignore
class GildedRoseTests {
    @Test
    fun `The quality is reduced by one every day`(){
        val item = Item("", 10, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(9)
    }

    @Test
    fun `The quality should never be negative`(){
        val item = Item("", 10, 0)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(0)
    }

    @Test
    fun `Sell in days is reduced by one every day`(){
        val item = Item("", 10, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.sellIn.shouldBe(9)
    }

    @Test
    fun `Sell in days can be negative`(){
        val item = Item("", 0, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.sellIn.shouldBe(-1)
    }

    @Test
    fun `Backstage increases quality over time`(){
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 20, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(11)
    }

    @Test
    fun `Backstage increases quality over time at double rate when sellin is near date`(){
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 10, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(12)
    }

    @Test
    fun `Backstage increases quality over time at double rate when sellin is almost due`(){
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 4, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(13)
    }

    @Test
    fun `Sulfuras sell in days is constant`(){
        val item = Item("Sulfuras, Hand of Ragnaros", 10, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.sellIn.shouldBe(10)
    }

    @Test
    fun `Backstage quality reset after sell in date comes`(){
        val item = Item("Backstage passes to a TAFKAL80ETC concert", 0, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(0)
    }

    @Test
    fun `Brie increases quality over time`(){
        val item = Item("Aged Brie", 10, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(11)
    }

    @Test
    fun `Brie increases quality over time up to 50`(){
        val item = Item("Aged Brie", 10, 50)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        item.quality.shouldBe(50)
    }


    @Ignore
    @Test
    fun `Brie increments quality past sell in days`(){
        val item = Item("Aged Brie", 0, 10)
        val app = GildedRose(arrayOf(item))

        app.updateQuality()

        //TODO: this is a bug, it should be 11, fix it today! (by: Leeroy)
        item.quality.shouldBe(11)
    }
}